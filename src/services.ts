import { AuthService } from "./services/AuthService";
import { MusicSearchService } from "./services/MusicSearchService";
import React from "react";
import { Album } from "./models/Album";

export const authService = new AuthService(
  "https://accounts.spotify.com/authorize",
  "70599ee5812a4a16abd861625a38f5a6",
  "token",
  "http://localhost:3000/"
);

// https://developer.spotify.com/dashboard/
// placki@placki.com
// ******

export const musicSearch = new MusicSearchService(authService);

export const musicSearchContext = React.createContext({
  onSearch(query: string) {},
  results: [] as Album[],
  loading:false,
  error: ""
});

// musicSearchContext.Consumer
// musicSearchContext.Provider
