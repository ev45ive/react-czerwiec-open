import { createStore, combineReducers } from "redux";
import { counterReducer as counter } from "./reducers/counterReducer";
import { SearchState, searchReducer as search } from "./reducers/SearchReducer";
import {
  playlists,
  PlaylistsState,
  loadPlaylists
} from "./reducers/PlaylistsReducer";

export interface AppState {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
}

const rootReducer = combineReducers({
  counter,
  search,
  playlists
});

export const store = createStore(
  rootReducer,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

store.dispatch(
  loadPlaylists([
    {
      id: 123,
      name: "React Hits",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "React TOP20",
      favorite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of React",
      favorite: true,
      color: "#00ffff"
    }
  ])
);
