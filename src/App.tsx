import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { MusicSearch } from "./views/MusicSearch";
import Playlists from "./views/Playlists";
import { Route, Switch, Redirect } from "react-router";
import { Link, NavLink } from "react-router-dom";
// import Placki from "./views/Playlists";

const App: React.FC = () => {
  return (
    <div className="App">
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/" activeClassName="placki active" exact={true}>
            Music App
          </NavLink>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">
                  Search
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <Switch>
          <Redirect from="/" to="/playlists" exact={true} />
          <Route path="/playlists/:id" component={Playlists} />
          <Route path="/playlists" component={Playlists} />
          <Route path="/search" component={MusicSearch} />
          <Route path="/404" render={() => <div>404 not found!</div>} />
          <Redirect from="/access_token**" to="/" />
          <Redirect from="*" to="/404" />
        </Switch>
      </div>
    </div>
  );
};

export default App;
