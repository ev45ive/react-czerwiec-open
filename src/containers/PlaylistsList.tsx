import { connect } from "react-redux";
import { AppState } from "../store";
import { ItemsList } from "../components/ItemsList";
import { Playlist } from "../models/Playlist";
import {
  selectPlaylist,
  selectedPlaylistSelector
} from "../reducers/PlaylistsReducer";

type TStateProps = {
  playlists: Playlist[];
  selected: Playlist | null;
};

type TDispatchProps = {
  onSelect(selected: Playlist | null): void;
};

const connector = connect<TStateProps, TDispatchProps, {}, AppState>(
  state => ({
    playlists: state.playlists.playlists,
    selected: selectedPlaylistSelector(state)
  }),
  dispatch => ({
    onSelect(selected: Playlist) {
      dispatch(selectPlaylist(selected && selected.id));
      // own.history.push('/url/'+selected.id)
    }
  })
);

export const PlaylistsList = connector(ItemsList);

// export const PlaylistsGrid = connector(ItemsGrid );
// export const PlaylistsChart = connector(ItemsChart);
