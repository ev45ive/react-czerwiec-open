import { connect } from "react-redux";
import { AppState } from "../store";
import { searchAlbums } from "../reducers/SearchReducer";
import { Album } from "../models/Album";
import { SearchField } from "../components/SearchField";
import { SearchResults } from "../components/SearchResults";
import { withRouter, RouteComponentProps } from "react-router";

const connector = connect<
  { results: Album[] },
  {
    onSearch(query: string): void;
  },
  RouteComponentProps<{}>,
  AppState
>(
  (state, own) => ({
    results: state.search.results,
    query: new URLSearchParams(own.location.search).get("query")
  }),
  // dispatch => ({
  //   onSearch(query) {
  //     searchAlbums(dispatch)(query);
  //   }
  // })
  dispatch => ({
    onSearch: searchAlbums(dispatch)
  })
  // (state,dispatch,props) => ({})
);

export const ConnectedSearch = withRouter(connector(SearchField));
export const ConnectedResults = withRouter(connector(SearchResults));
