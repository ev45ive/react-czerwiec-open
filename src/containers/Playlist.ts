import { connect } from "react-redux";
import { AppState } from "../store";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { updatePlaylist } from "../reducers/PlaylistsReducer";
import {
  playlists,
  selectedPlaylistSelector
} from "../reducers/PlaylistsReducer";
import { bindActionCreators } from "redux";
import { withRouter, RouteComponentProps } from "react-router";

const connector = connect<
  {
    playlist: Playlist;
  },
  {
    onSave(draft: Playlist): void;
  },
  {
    onEdit?(): void;
    onCancel?(): void;
  } & RouteComponentProps<{ id: string }>,
  AppState
>(
  (state, own) => ({
    playlist: state.playlists.playlists.find(p => p.id == +own.match.params.id)! // selectedPlaylistSelector(state)!
  }),
  // dispatch => ({
  //   onSave(draft: Playlist) {
  //     dispatch(updatePlaylist(draft));
  //   },
  // })
  dispatch =>
    bindActionCreators(
      {
        onSave: updatePlaylist
      },
      dispatch
    )
);

export const SelectedPlaylist = withRouter(connector(PlaylistDetails));
export const SelectedPlaylistForm = withRouter(connector(PlaylistForm))
