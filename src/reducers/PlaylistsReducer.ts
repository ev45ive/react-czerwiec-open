import { Action, Reducer, ActionCreator } from "redux";
import { Playlist } from "../models/Playlist";
import { AppState } from "../store";

export type PlaylistsState = {
  playlists: Playlist[];
  selected: Playlist["id"] | null;
  query: string;
};

interface PLAYLISTS_LOADED extends Action<"PLAYLISTS_LOADED"> {
  payload: Playlist[];
}

interface PLAYLISTS_SELECTED extends Action<"PLAYLISTS_SELECTED">{
  type: "PLAYLISTS_SELECTED";
  payload: Playlist["id"] | null;
}

interface PLAYLISTS_UPDATED extends Action<"PLAYLISTS_UPDATED"> {
  payload: Playlist;
}
type Actions = PLAYLISTS_LOADED | PLAYLISTS_SELECTED | PLAYLISTS_UPDATED;

export const playlists: Reducer<PlaylistsState, Actions> = (
  state = {
    playlists: [],
    selected: null,
    query: ""
  },
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_LOADED":
      return { ...state, playlists: action.payload };
    case "PLAYLISTS_SELECTED":
      return { ...state, selected: action.payload };
    case "PLAYLISTS_UPDATED":
      return {
        ...state,
        playlists: state.playlists.map(p =>
          p.id === action.payload.id ? action.payload : p
        )
      };
    default:
      return state;
  }
};

export const loadPlaylists: ActionCreator<PLAYLISTS_LOADED> = (
  payload: Playlist[]
) => ({
  type: "PLAYLISTS_LOADED",
  payload
});

export const selectPlaylist: ActionCreator<PLAYLISTS_SELECTED> = (
  payload: Playlist["id"] | null
) => ({
  type: "PLAYLISTS_SELECTED",
  payload
});

export const updatePlaylist: ActionCreator<PLAYLISTS_UPDATED> = (
  payload: Playlist
) => ({
  type: "PLAYLISTS_UPDATED",
  payload
});

(window as any).loadPlaylists = loadPlaylists;
(window as any).selectPlaylist = selectPlaylist;

// actions.js:
// const PLAYLISTS_LOADED = "PLAYLISTS_LOADED";
//
// enum Types {
//   PLAYLISTS_LOADED = "[Playlists] Items Loaded"
// }

// https://redux-actions.js.org/introduction/tutorial

// https://redux.js.org/recipes/structuring-reducers/refactoring-reducer-example

export function selectedPlaylistSelector(state: AppState): Playlist | null {
  return (
    state.playlists.playlists.find(p => p.id === state.playlists.selected) ||
    null
  );
}
