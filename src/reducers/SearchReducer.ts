import { Album } from "../models/Album";
import { ActionCreator, Reducer, Dispatch } from "redux";
import { musicSearch } from "../services";

export type SearchState = {
  results: Album[];
  query: string;
  loading: boolean;
  error: string;
};

interface SEARCH_START {
  type: "SEARCH_START";
  payload: string;
}

interface SEARCH_SUCCESS {
  type: "SEARCH_SUCCESS";
  payload: Album[];
}

interface SEARCH_FAILED {
  type: "SEARCH_FAILED";
  payload: Error;
}

type Actions = SEARCH_START | SEARCH_SUCCESS | SEARCH_FAILED;

export const searchReducer: Reducer<SearchState, Actions> = (
  state = {
    error: "",
    loading: false,
    results: [],
    query: ""
  },
  action
) => {
  switch (action.type) {
    case "SEARCH_START":
      return { ...state, loading: true, error: "", results: [] };
    case "SEARCH_SUCCESS":
      return { ...state, results: action.payload, loading: false };
    case "SEARCH_FAILED":
      return {
        ...state,
        loading: false,
        error: action.payload.message
      };
    default:
      return state;
  }
};

const searchStart: ActionCreator<SEARCH_START> = (payload: string) => ({
  type: "SEARCH_START",
  payload
});
const searchSuccess: ActionCreator<SEARCH_SUCCESS> = (payload: Album[]) => ({
  type: "SEARCH_SUCCESS",
  payload
});
const searchFailed: ActionCreator<SEARCH_FAILED> = (payload: Error) => ({
  type: "SEARCH_FAILED",
  payload
});

export const searchAlbums = (dispatch: Dispatch<Actions>) => (
  query: string
) => {
  // Start
  dispatch(searchStart(query));
  //
  musicSearch
    .search(query)
    // Success
    .then(results => dispatch(searchSuccess(results)))
    // Error
    .catch(error => dispatch(searchFailed(error)));
};

// export const searchAlbumsAsync = async (
//   dispatch: Dispatch<Actions>,
//   query: string
// ) => {
//   try {
//     dispatch(searchStart(query));
//     const results = await musicSearch.search(query);
//     dispatch(searchSuccess(results));
//   } catch (error) {
//     dispatch(searchFailed(error));
//   }
// };
