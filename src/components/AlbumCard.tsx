import React from "react";
import { Album } from "../models/Album";
type P = {
  album: Album;
  className?: string;
};

export const AlbumCard: React.FunctionComponent<P> = ({ album, className }) => (
  <div className={"card " + className}>
    <img src={album.images[0].url} className="card-img-top" alt=""/>

    <div className="card-body">
      <h5 className="card-title">{album.name}</h5>
    </div>
  </div>
);
