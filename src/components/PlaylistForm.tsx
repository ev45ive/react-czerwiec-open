import React from "react";
import { Playlist } from "../models/Playlist";
import { RouteComponentProps } from "react-router";

type State = {
  playlist: Playlist;
};

type Props = {
  playlist: Playlist | null;
  onCancel(): void;
  onSave(draft: Playlist): void;
} & RouteComponentProps<{ id: string }>;

export class PlaylistForm extends React.PureComponent<Props, State> {
  state = {
    playlist: {} as Playlist
  };

  handleFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    const fieldName = e.target.name;

    // (this.state.playlist as any)[fieldName] = value;

    this.setState({
      playlist: {
        ...this.state.playlist,
        [fieldName]: value
      }
    });
  };

  save = () => {
    this.props.onSave(this.state.playlist);
  };

  render() {
    console.log("render");
    return (
      <div>
        <h3>Form</h3>

        <div className="form-group">
          <label>Name:</label>
          <input
            ref={this.inputRef}
            type="text"
            className="form-control"
            onChange={this.handleFieldChange}
            value={this.state.playlist.name}
            name="name"
          />
          {255 - this.state.playlist.name.length} / 255
        </div>

        <div className="form-group">
          <label>Favourite</label>
          <input
            type="checkbox"
            onChange={this.handleFieldChange}
            checked={this.state.playlist.favorite}
            name="favorite"
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            onChange={this.handleFieldChange}
            value={this.state.playlist.color}
            name="color"
          />
        </div>
        <input
          type="button"
          value="Cancel"
          onClick={this.props.onCancel}
          className="btn btn-danger"
        />
        <input
          type="button"
          value="Save"
          onClick={this.save}
          className="btn btn-success"
        />
      </div>
    );
  }

  inputRef = React.createRef<HTMLInputElement>();

  constructor(props: Props) {
    super(props);
    console.log("constructor", arguments);
  }

  componentDidMount() {
    console.log("componentDidMount", arguments);
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  // shouldComponentUpdate(nextProps: Props, nextState: State) {
  //   console.log("shouldComponentUpdate");
  //   // return true;
  //   return (
  //     this.props.playlist != nextProps.playlist ||
  //     this.state.playlist != nextState.playlist
  //   );
  // }

  static getDerivedStateFromProps(newProps: Props, nextState: State) {
    console.log("getDerivedStateFromProps", arguments);
    return {
      playlist:
        // Get playlist from parent only if different ID
        (newProps.playlist &&
          (newProps.playlist.id !== nextState.playlist.id
            ? newProps.playlist
            : nextState.playlist)) ||
        null
    };
  }

  getSnapshotBeforeUpdate(prevProps: Props, prevState: State) {
    console.log("getSnapshotBeforeUpdate", arguments);
    return /* snapshot */ { placki: 123 };
  }

  componentDidUpdate(prevProps: Props, prevState: State, snapshot: any) {
    console.log("componentDidUpdate", arguments);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount", arguments);
  }
}
