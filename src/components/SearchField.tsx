import React from "react";
import { RouteComponentProps } from "react-router";

// https://getbootstrap.com/docs/4.3/components/input-group/#button-addons

type Props = {
  placeholder?: string;
  onSearch(query: string): void;
  query?: string;
} & RouteComponentProps<{}>;
type State = { query: string };

export class SearchField extends React.PureComponent<Props, State> {
  state = {
    query: ""
  };

  componentDidMount() {
    if (this.props.query) {
      this.props.onSearch(this.props.query);
    }
  }

  static getDerivedStateFromProps(nextProps: Props, nextState: State) {
    return {
      // query: nextState.query || nextProps.query
    };
  }

  search = () => {
    this.props.onSearch(this.state.query);
    // this.props.history.push("/search?query=" + this.state.query);
    this.props.history.replace("/search?query=" + this.state.query);
  };

  timeout!: NodeJS.Timeout;

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ query: event.target.value });

    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.search();
    }, 400);
  };

  render() {
    return (
      <div className="input-group mb-3">
        <input
          placeholder={this.props.placeholder || "Search"}
          type="text"
          className="form-control"
          value={this.state.query}
          onChange={this.handleChange}
        />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={this.search}>
            Search
          </button>
        </div>
      </div>
    );
  }
}
