import React from "react";
import { Playlist } from "../models/Playlist";
import { Card } from "./Card";
import { RouteComponentProps } from "react-router";

type Props = {
  playlist: Playlist | null;
  onEdit(): void;
} & RouteComponentProps<{ id: string }>;

export const PlaylistDetails = React.memo((props: Props) => {
  console.log(props);
  debugger;
  if (!props.playlist) {
    return <p>Please select playlist</p>;
  }

  return (
    <div>
      <Card>
        <dl>
          <dt>Name:</dt>
          <dd>{props.playlist.name}</dd>
          <dt>Favourite</dt>
          <dd>{props.playlist.favorite ? "Yes" : "No"} </dd>
          <dt>Color</dt>
          <dd
            style={{
              backgroundColor: props.playlist.color,
              color: props.playlist.color
            }}
          >
            {props.playlist.color}
          </dd>
        </dl>
        <input
          type="button"
          value="Edit"
          onClick={props.onEdit}
          className="btn btn-info"
        />
      </Card>
    </div>
  );
});

// PlaylistDetails.propTypes = {
//   playlist:
// }
