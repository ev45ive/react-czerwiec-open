import React from "react";

export const Card: React.FunctionComponent = props => {
  return (
    <div className="card">
      <div className="card-body">{props.children}</div>
    </div>
  );
};

// React.createElement('div',{},'Children content')
