import React from "react";
import { AlbumCard } from "./AlbumCard";
import { Album } from "../models/Album";
import styles from "./SearchResults.module.css";

type P = {
  results: Album[];
};

export const SearchResults: React.FunctionComponent<P> = props => {
  return (
    <div className="card-group">
      {props.results.map(result => (
        <AlbumCard className={styles.card} album={result} key={result.id} />
      ))}
    </div>
  );
};
