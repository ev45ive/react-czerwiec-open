import React from "react";
import { Playlist } from "../models/Playlist";

type State = {};

type Props = {
  playlists: Playlist[];
  selected: Playlist | null;
  onSelect(selected: Playlist | null): void;
};

export class ItemsList extends React.PureComponent<Props, State> {
  select(selected: Playlist) {
    this.props.onSelect(
      this.props.selected && this.props.selected.id === selected.id
        ? null
        : selected
    );
  }

  render() {
    const selected = this.props.selected;
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((p, index) => (
            <div
              className={`list-group-item ${p === selected ? "active" : ""}`}
              key={p.id}
              onClick={() => this.select(p)}
            >
              {index + 1}. {p.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
