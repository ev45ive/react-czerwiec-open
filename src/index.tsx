import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { authService } from "./services";
import { MusicSearchProvider } from "./services/MusicSearchProvider";

// Redux Store
import { store } from "./store";
import { Provider } from "react-redux";

// Router
// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

// Auth
authService.getToken();

(window as any).store = store;
(window as any).React = React;
(window as any).ReactDOM = ReactDOM;

// App
ReactDOM.render(
  <Router>
    <Provider store={store}>
      <MusicSearchProvider>
        <App />
      </MusicSearchProvider>
    </Provider>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// const p = (text: string) => {
//   return React.createElement("p", {}, text);
// };

// const ps = texty.map((text, index) => p(index + 1 + ". " + text));

// const div = React.createElement("div", {}, ps);

// const texty = ["Ala lubi placki", "I psy", "i Rybki"];

// const Paragraph = (props: any) => <p>{props.index}. {props.text}</p>;

// // const div = React.createElement("div", {}, "Ala ma kota");
// // const div = <div>Ala ma kota {texty.map(text => Paragraph(text))}</div>;

// const div = <div>Ala ma kota {texty.map((text,index) => <Paragraph text={text} index={index} />)}</div>;

// ReactDOM.render(div, document.getElementById("root"));

// declare global {
//   interface Window {
//     React: any;
//   }
// }
