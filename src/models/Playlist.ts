// export class Playlist{
//   name:string
// }
// new Playlist()

export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  /**
   * Colox in HEX
   */
  color: string;
  // tracks: Array<Track>;
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}

// const x:Playlist = {}
