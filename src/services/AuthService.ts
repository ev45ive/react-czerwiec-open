export class AuthService {
  private token = "";

  constructor(
    private auth_url: string | null,
    private client_id: string,
    private response_type: string,
    private redirect_uri: string
  ) {
    this.token = JSON.parse(sessionStorage.getItem("token")!);

    const hash = window.location.hash;
    if (!this.token && hash) {
      const match = hash.match(/access_token=(.*?)&/);
      this.token = (match && match[1]) || "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }
  }

  authorize() {
    sessionStorage.removeItem("token");
    const url =
      `${this.auth_url}?` +
      `client_id=${this.client_id}` +
      `&response_type=${this.response_type}` +
      `&redirect_uri=${this.redirect_uri}`;

    window.location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}

// const λ = "😋";
