import React from "react";
import { musicSearchContext, musicSearch } from "../services";
import { Album } from "../models/Album";

type State = { results: Album[]; error: string; loading: boolean };

export class MusicSearchProvider extends React.Component {
  state: State = {
    results: [],
    loading: false,
    error: ""
  };

  onSearch = (query: string) => {
    this.setState({ loading: true, error: "" });
    musicSearch
      .search(query)
      .then(results => this.setState({ results }))
      .catch(error => this.setState({ error, results: [] }))
      .finally(() => this.setState({ loading: false }));
  };

  render() {
    return (
      <musicSearchContext.Provider
        value={{
          onSearch: this.onSearch,
          results: this.state.results,
          error: this.state.error,
          loading: this.state.loading
        }}
      >
        {this.props.children}
      </musicSearchContext.Provider>
    );
  }
}
