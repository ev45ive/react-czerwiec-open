import { AuthService } from "./AuthService";
import { Album } from "../models/Album";
import axios from "axios"; // npm i axios --save
import { authService } from "../services";

// axios.interceptors.request.use(config => {
//   config.headers["Authorization"] = "Bearer " + authService.getToken();
//   return config
// });

type AlbumsResponse = {
  albums: { items: Album[] };
};

export class MusicSearchService {
  constructor(private auth: AuthService) {}

  search(query: string = "batman") {
    return axios
      .get<AlbumsResponse>(`https://api.spotify.com/v1/search`, {
        params: {
          type: "album",
          q: query
        },
        headers: {
          Authorization: "Bearer " + this.auth.getToken()
        }
      })
      .then(resp => resp.data.albums.items)
      .catch(err => {
        return Promise.reject(err.response.data.error.message);
      });
  }

  // search(query: string = "batman") {
  //   return fetch(`https://api.spotify.com/v1/search?type=album&q=${query}`, {
  //     headers: {
  //       Authorization: "Bearer " + this.auth.getToken()
  //     }
  //   })
  //     .then(r => (r.status >= 400 ? Promise.reject(r) : r))
  //     .then(r => r.json())
  //     .catch(err => {
  //       debugger;
  //       Promise.reject(err.error);
  //     })
  //     .then(resp => resp.albums.items as Album[]);
  // }

  // ===
  // async asyncSearch(query: string) {
  //   var response = await fetch(
  //     `https://api.spotify.com/v1/search?type=album&q=${query}`,
  //     {
  //       headers: {
  //         Authorization: "Bearer " + this.auth.getToken()
  //       }
  //     }
  //   );
  //   const json = await response.json();
  //   console.log(json);
  // }
}
