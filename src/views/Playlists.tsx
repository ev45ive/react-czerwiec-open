import React, { useState } from "react";
import { PlaylistsList } from "../containers/PlaylistsList";
import { SelectedPlaylist, SelectedPlaylistForm } from "../containers/Playlist";

enum Modes {
  show = "show",
  edit = "edit"
}

type State = {
  mode: Modes;
};

export class Playlists extends React.Component<{}, State> {
  state: State = {
    mode: Modes.show
  };

  edit = () => {
    this.setState({ mode: Modes.edit });
  };

  cancel = () => {
    this.setState({ mode: Modes.show });
  };

  render() {
    
    return (
      <div className="row">
        <div className="col">
          <PlaylistsList />
        </div>
        <div className="col">
          {this.state.mode === Modes.show && (
            <SelectedPlaylist onCancel={this.cancel} onEdit={this.edit} />
          )}
          {this.state.mode === Modes.edit && (
            <SelectedPlaylistForm onCancel={this.cancel} />
          )}
        </div>
      </div>
    );
  }
}

export default Playlists;
