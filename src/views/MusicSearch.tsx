import React, { Component } from "react";
import { ConnectedSearch, ConnectedResults } from "../containers/MusicSearch";
import { RouteComponentProps } from "react-router-dom";

type Props = { placki?: string } & RouteComponentProps<{}>;

export const MusicSearch = (props: Props) => {
  // const query = new URLSearchParams(props.location.search).get("query");

  return (
    <div>
      <div className="row">
        <div className="col">
          <ConnectedSearch placeholder="Search albums" />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <ConnectedResults />
        </div>
      </div>
    </div>
  );
};
