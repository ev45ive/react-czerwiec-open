var inc = (payload = 1) => ({type:'INC', payload});
var dec = (payload = 1) => ({type:'DEC', payload})

var reducer = (state, action) =>{
	switch(action.type){	
        case 'INC': return {...state, value: state.value + action.payload };
        case 'DEC': return {...state, value: state.value - action.payload };
        default: return state;
    }
}

// state = reducer(inc, state)
// state = reducer(dec, state)
// state = reducer(inc, state)
// state = reducer(inc, state)

[inc(),inc(1),inc(2),dec(3),dec(),inc()].reduce(reducer ,{ value: 0, placki:123})